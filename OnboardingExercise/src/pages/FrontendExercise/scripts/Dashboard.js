import { defineComponent, ref } from "vue";
import EssentialLink from "components/EssentialLink.vue";

const linksList = [
  {
    title: "menu 1",
    icon: "account_balance",
    route_name: "menu-1",
  },
  {
    title: "menu 2",
    icon: "account_balance",
    route_name: "menu-2",
  },
  {
    title: "menu 3",
    icon: "account_balance",
    route_name: "menu-3",
  },
];

export default defineComponent({
  name: "MainLayout",
  components: {
    EssentialLink,
  },

  setup() {
    return {
      essentialLinks: linksList,
    };
  },
});
