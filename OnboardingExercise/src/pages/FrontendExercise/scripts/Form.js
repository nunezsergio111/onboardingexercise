import { defineComponent, ref } from "vue";
import EssentialLink from "components/EssentialLink.vue";
import axios from "axios";

import { useRouter } from "vue-router";

const linksList = [
  {
    title: "menu 1",
    icon: "account_balance",
    route_name: "menu-1",
  },
  {
    title: "menu 2",
    icon: "account_balance",
    route_name: "menu-2",
  },
  {
    title: "menu 3",
    icon: "account_balance",
    route_name: "menu-3",
  },
];

export default defineComponent({
  name: "MainLayout",
  components: {
    EssentialLink,
  },

  setup() {
    const router = useRouter();
    const name = ref("");
    const employee_id = ref("");
    const email = ref("");
    const task = ref("");
    const description = ref("");
    const status = ref("");
    const address = ref("");

    const addEmployee = async () => {
      console.log("test");
      // Save data locally (for immediate UI update)
      const employees = {
        name: name.value,
        employee_id: employee_id.value,
        email: email.value,
        task: task.value,
        description: description.value,
        status: status.value,
        address: address.value,
      };

      console.log("test: ", +employees);
      try {
        // Save data to the server
        await saveUserToServer(employees);
        navigateToTable();
        clearForm();
      } catch (error) {
        console.error("Error saving user:", error);
      }
    };

    const saveUserToServer = async (employees) => {
      // Save user data to the server using Axios
      await axios.post("http://localhost:3000/employees", employees);
      console.log("Added");
    };

    const clearForm = () => {
      // Clear form fields after submission
      name.value = "";
      email.value = "";
      employee_id.value = "";
      task.value = "";
      description.value = "";
      status.value = "";
      address.value = "";
    };

    const navigateToTable = () => {
      router.push("/menu-3");
    };

    return {
      essentialLinks: linksList,
      name,
      employee_id,
      email,
      task,
      description,
      navigateToTable,
      status,
      address,
      addEmployee,
    };
  },
});
