import { defineComponent, ref } from "vue";
import EssentialLink from "components/EssentialLink.vue";
import axios from "axios";
import { useRouter } from "vue-router";

const linksList = [
  {
    title: "menu 1",
    icon: "account_balance",
    route_name: "menu-1",
  },
  {
    title: "menu 2",
    icon: "account_balance",
    route_name: "menu-2",
  },
  {
    title: "menu 3",
    icon: "account_balance",
    route_name: "menu-3",
  },
];

export default defineComponent({
  name: "MainLayout",
  components: {
    EssentialLink,
  },

  setup() {
    const router = useRouter();
    let rows = ref([]);
    // Define table columns
    const columns = ref([
      {
        name: "Employee ID",
        required: true,
        label: "Employee ID",
        align: "left",
        field: "employee_id",
      },
      {
        name: "Name",
        align: "left",
        label: "Name",
        field: "name",
      },
      {
        name: "Email",
        align: "left",
        label: "Email",
        field: "email",
      },
      {
        name: "Status",
        align: "left",
        label: "Status",
        field: "status",
      },
      {
        name: "Address",
        align: "left",
        label: "Address",
        field: "address",
      },
      {
        name: "",
        align: "",
        label: "",
        field: "",
      },
    ]);

    const getRecords = () => {
      axios.get("http://localhost:3000/employees").then((response) => {
        console.log(response.data);
        rows.value = response.data;
      });
      console.log("test");
    };

    const navigateToForm = () => {
      router.push("/addEmployee");
      console.log("test");
    };

    getRecords();

    return {
      rows,
      columns,
      getRecords,
      navigateToForm,
      essentialLinks: linksList,
    };
  },
});
