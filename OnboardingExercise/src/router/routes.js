const routes = [
  {
    path: "/",
    redirect: {
      name: "menu-1",
    },
    component: () => import("layouts/MainLayout.vue"),
    children: [
      {
        path: "menu-1",
        name: "menu-1",
        component: () => import("pages/FrontendExercise/DashboardPage.vue"),
      },
      {
        path: "menu-2",
        name: "menu-2",
        component: () => import("pages/FrontendExercise/EmployeeList.vue"),
      },
      {
        path: "addEmployee",
        name: "addEmployee",
        component: () => import("pages/FrontendExercise/Form.vue"),
      },
      {
        path: "menu-3",
        name: "menu-3",
        component: () => import("pages/FrontendExercise/EmployeeList2.vue"),
      },
    ],
  },
];

export default routes;
